# ReactJS

## 01 Can you explain the advantages of using JSX?
* JSX is similar to html. 
* JSX stands for JavaScript XML.
* JSX allows us to write HTML in React.
* JSX makes it easier to write and add HTML in React.
* Easy to understand who are familiar with html.
* SX allows us to write HTML elements in JavaScript and place them in the DOM without any createElement() and/or appendChild() methods.
* JSX converts HTML tags into react elements.

## 02  Can you explain what is React Element?
* In React, a React Element is an object that represents a DOM element.
* Rect element's are creates a JSX syntax, babel transpiler will convert JSX to javascript objects.Then browser will understand.

        React.createElement()

        example:

            <h1>good morning</h1> - JSX syntax

            const h1Element = React.createElement('h1',null,'good morning') - JavaScript Syntax
            

## 03 Can you list all the situation in which React re-renders the component?
* Component will re-render when state changes.
* We can change state by 
  1. setState() - class component's
  2. useContext() value when changes
  3. useReducer() - reducer hook
  4. redux & reduxJS/toolkit - javascript library
* When parent component re-render what are the children components having parent data as a props that children components also re-render.
* When you use React.StrictMode components will render two times every state changes time.

## 04  Can you explain when we need a functional and when we need a class component?
* The choice between these two approaches depends on the requirements of the component and the preferences of the developer.
* Functional components are simpler and easier to read and write. They are pure functions that receive some props and return a React element, without any side effects.
* Functional components are recommended when we need to display simple UI elements that do not have any state or do not require any lifecycle methods.
* But class components different with functional it's have own state and life-cycle methods.
* When you don't need of state maintaining on that situation prefer functional components.
* When you need state management & DOM manipulation's & use of life-cycle method's of a component prefer Class based components.
* But in reactJS new version functional components have Hooks, It fulfill all methods of class components have.

## 05 Can you explain the difference between a functional and class component?
* class components have own state management.
* Class components have access to several lifecycle methods such as componentDidMount(), componentDidUpdate(), and componentWillUnmount(). These methods allow us to control the behavior of the component at various stages of its lifecycle.
* Class components are defined as JavaScript classes that extend the React.Component class. They have a render() method that returns a React element.
* Functional components are defined as JavaScript functions that receive props as their argument and return a JSX.
* Functional components do not have state. They can only receive props as their input and return a JSX based on them.
* Functional components are generally more lightweight and faster than class components. Because do not have any state and life-cycle methods.
*  We should use class components when we need to maintain some internal state, access lifecycle methods, or handle complex logic.

## 06 Can you explain why the component name should starts with uppercase?
* In reactJS, mandatory to write first letter Capitalize name of component for functional and class based components.
* Because if you write a small letter's the reactJS assume that Html tag, it won't recognise that as a component.

        function navbar() {

        }

        the html <tag> are small letters.

* We should write first letter capitalize that react will differentiate html tags and user defined Component's.

      function Navbar() {
        
      }

* In summary, capitalizing the first letter of the component name is a convention in React that helps to differentiate between user-defined components and HTML elements. It also helps to avoid naming conflicts and makes the code more readable and maintainable.

## 07 Can you explain why we need ReactDOM?
* React is a JavaScript library for building user interfaces. It allows developers to create reusable UI components and manage the state of those components.
* React is just a library for managing the virtual DOM, which is a lightweight representation of the actual DOM.
* The main purpose of ReactDOM is to render the React components to the actual DOM. It provides a set of methods for manipulating the DOM.
  
           ReactDOM.render()

* ReactDOM also provides a way to handle user events, such as clicks and keyboard input, and update the virtual DOM accordingly. When the virtual DOM is updated, ReactDOM takes care of updating the actual DOM to reflect those changes.

## 08 Can you explain the difference between state and props?
* Both state and props are used to store and manage data, but they have different role.
* State is an internal data store for a component that allows the component to manage its own data. It is used for data that changes over time within a component, such as user input, form submissions, or any other event that causes a change in the component's behavior.
* State is managed by the component itself and can be updated using the **setState()** method. When state is updated, React automatically re-renders the component and its child components to reflect the changes.
* Props passing data from a parent component to its child components. Props are read-only and cannot be changed by the child component. They are used to pass data down the component tree to child components that need to access it.
* state is used for internal data management within a component, while props are used to pass data down the component tree from parent to child components.

## 09 Can you list all the difference between JSX and HTML?
* JSX is a syntax extension of JavaScript, HTML is a markup language. JSX allows you to write HTML-like syntax inside your JavaScript code, making it easier to create components and dynamically render data.
* In JSX we write class attribute className camelcase, but we write in html class only.
* In JSX, you can set inline styles using JavaScript objects, whereas in HTML, you use the style attribute and set styles using CSS syntax.
* HTML is a very important language in web development. Your code is either in HTML originally or compiles to it so browsers can read it. 
* JSX, on the other hand, means JavaScript Syntax Extension or JavaScript XML as some like to put it. It was created as a syntactic sugar for React.
* One of the major differences between HTML and JSX is that in JSX, you must return a single parent element.

## 10 Can you explain why we use PropTypes?
* PropTypes is a feature of React that allows developers to define the expected data types of the props passed to a component. It helps to ensure that the data being passed to a component is of the correct type, reducing the chances of bugs and making it easier to debug code.
* PropTypes provide a way to validate the types of props passed to a component.
* By defining the types and requirements of props in the PropTypes, other developers working on the same codebase can quickly understand how the component is supposed to be used.

## 11 Can you explain this statement setState is asynchronous in nature with an example?
* **setState()** is asynchronous in nature. This means that when you call setState(), React may not update the component's state immediately, and the component may not re-render right away.

        function Counter() {
          const [count,setState] = useState(0)

        handleClick = () => {
          setState(prev => prev+1)
          console.log(count)
        }

        render() {
          return (
            <div>
              <p>Count: count</p>
              <button onClick={handleClick}>
              Increment</button>
            </div>
          );
        }
      }

* In above example the state won't updates immediately. we will see that it still has its previous value. This is because setState() is asynchronous 

## 12 Can you explain why we should not update the state directly?
* Directly updating the state can cause unexpected changes in the application's behavior.If you update state directly it won't re-render component.
* Directly updating the state can also cause loss of data integrity. If there are any dependencies on the state, such as computed properties or derived state, they may not update correctly if the state is updated directly. 
* Directly updating the state can make it harder to debug code. If the state is updated in multiple places throughout the codebase, it can be difficult to track down where a bug originated.
* Recomended to state changes **setState()**.
  
## 13  Can you handle form validation in React?
* You can also create custom validation functions to validate form inputs. For example, you can create a function that checks if an email address is valid or a function that checks if a password meets certain complexity requirements. You can then call these functions on form submission to validate the inputs.
* we can use **required** keyword to field mandatory.
* We can write our own error messages using forms.
* There are many third-party libraries available to handle form validation in React, such as Formik, Yup, or React Hook Form. These libraries provide a range of features such as form submission handling, field validation, error messages, and more.

## 14 Can you explain what is Virtual DOM and how it works?
* The Virtual DOM react will crate it using actual DOM. It is a lightweight copy of the real DOM (Document Object Model) that exists in memory and is used by libraries and frameworks like React to optimize performance and minimize the number of updates required to render the UI.
* When a user interacts with a web application, such as clicking a button or typing in a text field, it triggers an update to the UI using virtual DOM.
* Instead of immediately updating the real DOM, React creates a lightweight representation of the updated UI in the form of a Virtual DOM.
* The Virtual DOM is a tree-like structure that represents the UI components and their properties.
* React then compares the new Virtual DOM with the previous Virtual DOM to determine the minimal set of changes required to update the real DOM.
* The advantage of using the Virtual DOM is that it allows for more efficient updates to the UI. Since the Virtual DOM is a lightweight representation of the real DOM.

## 15 Can you explain the difference between controlled and uncontrolled component?
* In ReactJS, components can be controlled or uncontrolled depending on how they handle user input and update their state.
* A controlled component is one where the value of the input element is controlled by React.The state of the component is updated in response to changes in the input value, and the component only re-renders when the state changes.
* The state of the component is managed by the parent component, which can pass down the state and handlers as props to the controlled component.

      import React, { useState } from 'react';

      function ControlledComponent() {
        const [name, setName] = useState('');

        const handleChange = (event) => setName(event.target.value);
       
        return (
          <div>
            <input type="text" value={name} onChange={handleChange} />
            <p>{name}</p>
          </div>
        );
      }
* An uncontrolled component is one where the value of the input element is handled by the browser.The state of the component is not managed by React, and the component can modify the DOM directly.

## 16 Can you explain why we need to pass key prop when displaying a list?
* In reactJS mandatory to pass a **key prop** when we using list, because react will identify which element has updated and which one deleted.
* This can significantly improve the performance of rendering a list of elements in React.
  
      const List = [.....]
      List.map(eachItem => {
        <Component key={eachItem.id} details={eachItem}>
      })
## 17 Can you explain different lifecycle methods and their use-cases?
* life cycle methods in reactJS
  *  componentDidMount()
  *  componentDidUpdate()
  * componentWillUnMount()
  * constructor()

* constructor() This first created, and is used to initialize the component's state and bind methods.
* componentDidUpdate() This is called after a component is updated, and is used to perform any necessary cleanup or update any external state.
* componentWillUnMount() This is called when a component is about to be unmounted, and is used to perform any necessary cleanup.

## 18 Can you list the reasons to use context in React and how it works?
* In reactJS we can pass props or data to child elements using react-Context.It will available data globally, that every child component will access that data.
* Why we use context in reactJS sometimes have no access on tree global data, on that situation you can export data using context.Then every child can 
* Passing data deeply into tree,updating data passed via context.
* If you have deeply nested components, you might find yourself passing props through several layers of components just to get them where they are needed. This can make your code harder to read and maintain. Context allows you to pass data down the component tree without having to explicitly pass it as a prop through every level of the tree.
* Sometimes you need to share data between components that are not directly related to each other in that situation we use context.

## 19 Can you explain the use case of React.Fragment?
* Using React.Fragment can help keep your HTML structure clean and can be particularly useful when you're rendering a list of items that need to be wrapped in an element without adding an extra element to the DOM.
* React.Fragment acts as a wrapper around the child elements, but doesn't add any additional elements to the DOM.

      function App() {
        return (
          <React.Fragment>
            <h1>Hello</h1>
            <p>World</p>
          </React.Fragment>
        );
      }
## 20 Can you explain if it is possible to loop inside the JSX give an example of how to do this?
* Yes it's possible to loop inside the JSX.
* By using **map()** we can loop inner JSX.you can use the JavaScript map() method to loop through an array of data and return a list of JSX elements.

        import React from 'react';
          function MyComponent(props) {
            const items = ['item 1', 'item 2', 'item 3'];  
            return (
              <div>
                <h1>List of Items:</h1>
                <ul>
                  {items.map((item, index) => (
                    <ComponentItem key={index}>{item}</ComponentItem>
                  ))}
                </ul>
              </div>
            );
          }

          export default MyComponent;

## 21 Can you explain the advantages of using Hooks?
* React Hooks are new feature of React introduced in version that allows developers to use state and other React features without writing a class component.
* you can use state and other React features without having to write a class component. This leads to cleaner and easier-to-understand code because the code is less.
* Reuse it across multiple components, making it easier to build composable and reusable UI components.
*  Hooks can help improve the performance of your React applications because they allow you to optimize the rendering of components. For example, by using the useMemo() and useCallback() Hooks, you can memoize expensive calculations and prevent unnecessary re-renders.
*   Hooks provide more flexibility when it comes to managing state and lifecycle events using useEffect().

## 22 Can you explain how to use useEffect?
### Usage:
* To fetch Data from API
* To stop timer's
* To Cancel subscription's
* To cancel or remove event litsener's
* Clean up & side effect's

Example:
    import React, { useState, useEffect } from 'react'

    function MouseContainer() {
        const [x, setX] = useState(0)
        const [y, setY] = useState(0)

        const logMousePosition = e => {
            console.log('mouse event')
            setX(e.clientX)
            setY(e.clientY)
        }

        useEffect(() => {
            window.addEventListener('mousemove', logMousePosition)

            return () => {
                window.removeEventListener('mousemove', logMousePosition)
            }
        },[])
        return (
            <div>
                <h1>Hooks - X - {x} Y - {y}</h1>
            </div>
        )
    }

    export default MouseContainer

* In class component's life-cycel method's ComponentDidMount,ComponentDidUpdate and ComponentWillUnmount.This method's will call different situation's like ComponentDidMount method will call when component "Mounted" (create) ComponentDidUpdate method will call when component "State change's" (state updated) ComponentWillUnmount method will call when component "UnMounted" (removed from DOM)
* The above all method's we can use in one function simply by using "useEffect" hook.
* Declaring dependecy array-[] we can customize useEffect hook behavior.
* We can return callback function to cleanup activities.

## 23 Can you list all the hooks we can use to manage state in React with example?
### useState()
* When the UI re-render on that time want a previous data to update current data, In that case we store that type data in state bu using useState hook.It will re-render when the store data will change.

           import React, { useState } from 'react';

            function Example() {

            const [count, setCount] = useState(0);

            return (
                <div>
                <p>You clicked {count} times</p>
                <button onClick={() => setCount(count + 1)}>
                    Click me
                </button>
                </div>
            )
            }
### useReducer()
* When our application grows in size, we will most likely deal with more complex state transitions, at which point we can be better off using useReducer.
* useReducer provides more predictable state transitions than useState, More important when state changes become so complex that you want to have one place to manage state, like the render function.
* t's take two parameters one is reducer function & second is initial state value.The reducer function will take tow arguments currentState & action returns a new state.By calling dispatch function we can perform action's on state.

        import React from 'react'

        const initialState = {
            firstCount: 0
        }

        const reducer = (state, action) => {
            switch (action.type) {
                case 'increment':
                    return { firstCount: state.firstCount + 1 }
                case 'decrement':
                    return { firstCount: state.firstCount - 1 }
                case 'reset':
                    return initialState
                default:
                    return state
            }
        }

        function CounterReducerTwo() {
            const [count, dispatch] = React.useReducer(reducer, initialState)
            return (
                <div>
                    Count {count.firstCount}
                    <button type='button' onClick={() => dispatch({ type: 'increment' })}>Increment</button>
                    <button type='button' onClick={() => dispatch({ type: 'decrement' })}>Decrement</button>
                    <button type='button' onClick={() => dispatch({ type: 'reset' })}>Reset</button>

                </div>
            )
        }

        export default CounterReducerTwo
## 24 Can you explain the difference between a DOM element and React element?
* DOM element is an object that represents an element in the browser's Document Object Model (DOM). The DOM is a tree-like structure that represents the structure of an HTML document. When you write HTML code, the browser creates a DOM tree based on that code. Each element in the DOM tree is represented by a DOM element object, which you can manipulate using JavaScript.
* React element is an object that represents a UI element in a React application. A React element is a lightweight object that contains information about a component and its props. React elements are not part of the DOM. Instead, React uses a virtual DOM to represent the UI. The virtual DOM is a lightweight representation of the actual DOM, which React uses to optimize updates and improve performance.

## 25 Can you explain what happens when any runtime error happens in the JSX? How can we fix that issue?
* Runtime error cause because of null values, undefined values and invalid arguments.When a runtime error occurs, the application will usually stop working and display an error message.
* Use debugging tools such as the browser console or a debugger to identify the source of the error. Once you have identified the source of the error, you can make changes to the code to fix the problem.
* Use error handling techniques such as try-catch blocks or error boundaries to handle errors in a more graceful way. This can help prevent the application from crashing and provide a better user experience.

## 26 Can you explain the rules of hooks?
* Hooks must be called at the top level of a functional component, and not inside loops, conditions, or nested functions. 
* Hooks can only be called from React functions, such as functional components, custom Hooks, or other React Hooks. They cannot be called from regular JavaScript functions or classes.
* Hooks can only be used in functional components, not in class components.
* you should not call the same Hook multiple times in the same component.
* If you have stateful logic that needs to be reused in several components, you can build your own custom Hooks.
* ## 27 Can you explain why we can put hooks in a if statement?
* Hooks should not be called conditionally, including inside if statements or other conditions.If Hooks are called conditionally, the order of Hook calls can change between renders, which can lead to unexpected behavior, bugs, or even crashes.
*  if a Hook that sets the state of a component is called conditionally, the component's state may not update correctly.
*  Instead of calling Hooks conditionally, you can use conditional rendering techniques in React, such as the ternary operator or logical operators.
*  If you need to use Hooks in a specific condition, you can extract the Hook call into a separate function or custom Hook and call it unconditionally in the component's render function. This ensures that the Hook is called in the correct order and avoids any unexpected behavior.

## 28  Can you explain what is the advantage of using React.StrictMode?
* It will render two times in react to know weather Components and function are pure functions or not.
* StrictMode is a tool that helps developers write better React code by detecting potential problems and providing helpful warnings in the development mode of the application.
* StrictMode also helps identify unsafe code patterns that may cause problems in production, such as writing to state or props directly, or using components in unexpected ways. 

## 29 Can you create the production build of a React app?
* Yes we can ceate entering **npm run build**, It will built a html,CSS,JS files for production.
* Once the build is complete, you can deploy the production build of your app to a web serve.
* In reactJS itself will do a production build.

## 30 Can you explain the difference between Development and Production?
* where developers write, test, and debug code. It's a sandboxed environment where developers can experiment and make changes to the codebase without affecting production.
* Production means released into the public as an official fully released delivery.
